"use strict";

var gulp         = require("gulp");
var gutil        = require("gulp-util");
var source       = require("vinyl-source-stream");
var buffer       = require("vinyl-buffer");
var browserify   = require("browserify");
var watchify     = require("watchify");
var uglifyify    = require("uglifyify");
var sourcemaps   = require('gulp-sourcemaps');
var sass         = require('gulp-sass');
var livereload   = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');

var src_path = './src/';
var assets_path = gutil.env.assets_path ? gutil.env.assets_path.replace(/\/?$/, '/') : null;
var assets_path_default = './build/puzzle/';
var bundle_path = gutil.env.bundle_path ? gutil.env.bundle_path.replace(/\/?$/, '/') : null;
var bundle_path_default = './build/';

function getBundler(isProd) {
  var transforms = [];
  if (isProd) {
    transforms.push([uglifyify, {global: true}]);
  }
  var bundler = browserify({
    entries:      [src_path + 'js/puzzle.js'],
    transform:    transforms,
    debug:        !isProd,
    standalone:   'Puzzle',
    cache:        {},
    packageCache: {}
  });
  return bundler;
}

gulp.task("bundle:js", function() {
  bundle_path = bundle_path || bundle_path_default;
  getBundler(true)
    .bundle()
    .on("error", gutil.log)
    .pipe(source("puzzle.js"))
    .pipe(gulp.dest(bundle_path));
  if (assets_path && assets_path !== assets_path_default) {
    gulp.src(assets_path_default + '**/*')
    .pipe(gulp.dest(assets_path))
  }
})

gulp.task("watch:js", function() {
  var bundler = getBundler(false);
  var watcher = watchify(bundler);
  rebundle();
  watcher
  .on("update", rebundle)
  .on("log", function(msg) {gutil.log('App: ' + msg)});
  function rebundle() {
    var updateStart = Date.now();
    gutil.log('Updating App JavaScript bundle');
    watcher
    .bundle()
    .on("error", gutil.log)
    .pipe(source("puzzle.js"))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(bundle_path_default));
  }
})

gulp.task('default', ['watch:js']);
gulp.task('bundle',['bundle:js']);