"use strict";

require('./polyfills');
var glslify = require('glslify');
window.THREE = require('./lib/three.fixed');
require('./lib/OrbitControls');
var points = require('./points');
var easings = require('./easings');

var materialsShared = {
  'pipe': new THREE.ShaderMaterial({
    uniforms: {
      uNumpoints: { type: "f" },
      uPosTex1: { type: "t" },
      uPosTex2: { type: "t" },
      uNormTex1: { type: "t" },
      uNormTex2: { type: "t" },
      morph: { type: "f" },
      envMap: { type: "t" },
      cubeMul: { type: "f" },
      cubeAddition: { type: "f" },
      addition: { type: "f" },
      mixWhite: { type: "f" },
      contrast: { type: "f" },
      opacity: { type: "f" },
      dataSide: { type: "f" },
    }, 
    vertexShader: glslify(__dirname + '/shaders/pipe.vert'),
    fragmentShader: glslify(__dirname + '/shaders/pipe.frag'),
    transparent: true,
  }),
  'pipeNoTex': new THREE.ShaderMaterial({
    uniforms: {
      envMap: { type: "t" },
      cubeMul: { type: "f" },
      cubeAddition: { type: "f" },
      addition: { type: "f" },
      mixWhite: { type: "f" },
      contrast: { type: "f" },
      opacity: { type: "f" },
    }, 
    vertexShader: glslify(__dirname + '/shaders/pipe_no_tex.vert'),
    fragmentShader: glslify(__dirname + '/shaders/pipe_no_tex.frag'),
    transparent: false,
  }),
};

var fallbackFiles = {
  large: [
    'fallback_large_0.png',
    'fallback_large_1.png',
    'fallback_large_2.png',
  ],
  small: [
    'fallback_small_0.png',
    'fallback_small_1.png',
    'fallback_small_2.png',
  ]
};

var curves = [];
var cubeShared;
var assetsPath;
var fallbacks;
var fallbackImgs = [];
var isIntelSafari = false;
var offsets = [
  new THREE.Vector3(-0.3, 0.3, 0),
  new THREE.Vector3(-10, 0.05, 0),
  new THREE.Vector3(-21, 1, 0)
];
var spherePositions = [
  new THREE.Vector3(-4.5, -1.2, 0),
  new THREE.Vector3(1.8, 2, 0),
  new THREE.Vector3(3.8, 1.1, 0)
];
var scale = 0.05;

var isTouchDevice = 'ontouchstart' in document.documentElement;
var glFeatures = {};
var webgl = (function () {
  try {
    var canvas = document.createElement( 'canvas' );
    var gl = window.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ) );
    glFeatures.singleFloat = !!gl.getExtension('OES_texture_float');
    glFeatures.singleFloatLinear = !!gl.getExtension('OES_texture_float_linear');
    glFeatures.vertexUnits = gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS);
    var renderInfo = gl.getExtension("WEBGL_debug_renderer_info");
    if (!!renderInfo) {
      var vendor = gl.getParameter(renderInfo.UNMASKED_VENDOR_WEBGL).toLowerCase();
      var ua = navigator.userAgent.toLowerCase(); 
      isIntelSafari = (vendor.indexOf('intel') >= 0) && (ua.indexOf('safari') > -1) && (ua.indexOf('chrome') < 0);
    }
    return !!gl && glFeatures.singleFloat && glFeatures.singleFloatLinear && (glFeatures.vertexUnits > 1);
  } catch (e) {
    return false;
  }
})();

function load(config) {
  assetsPath = config.assetsPath;
  fallbacks = config.fallbacks;
  return new Promise(function(resolve, reject) {
    if (webgl) {
      var cubeLoader = new THREE.CubeTextureLoader();
      var loadCube = function() {
        var path = assetsPath + '/cube/';
        var urls = [ path + "px.jpg", path + "nx.jpg",
                     path + "py.jpg", path + "ny.jpg",
                     path + "pz.jpg", path + "nz.jpg" ];
        cubeLoader.load(urls, function(cube) {
          cubeShared = cube;
          resolve();
        });
      };
      loadJSON(assetsPath + '/data.json', function(text) {
        var data = JSON.parse(text);
        var getCurve = function(obj, index) {
          var puzzleCurves = [];
          for (var n = 0; n < 2; n++) {
            var points = [];
            var num = obj[n].length / 3;
            var objn = obj[n];
            for (var i = 0; i < num; i++) {
              var _i = index === 0 ? num - 1 - i : i;
              var ind = _i * 3;
              var point = {};
              point.x = parseFloat(objn[ind]) + offsets[index].x / scale;
              point.y = parseFloat(objn[ind + 1]) + offsets[index].y / scale;
              point.z = parseFloat(objn[ind + 2]) + offsets[index].z / scale + index * 2;
              points.push(point);
            }
            if (num < 100) {
              points.push(point);
            }
            points.closedIndex = index === 1 ? 1 : 0 ;
            points.closed = n === points.closedIndex;
            var ind = n;
            if (index === 1) {
              ind = 1 - ind;
            }
            puzzleCurves[ind] = points;
          }
          return puzzleCurves;
        };
        curves.push(getCurve(data[0], 0));
        curves.push(getCurve(data[1], 1));
        curves.push(getCurve(data[2], 2));
        loadCube();
      });
    } else if (fallbacks && fallbacks.length) {
      var manager = new THREE.LoadingManager();
      manager.onLoad = function() {
        resolve();
      };
      manager.onError = function() {
        reject('Puzzle: fallbacks loading failed');
      };
      var loader = new THREE.ImageLoader(manager);
      for (var i = 0; i < fallbacks.length; i++) {
        (function(i) {
          loader.load(assetsPath + '/fallback/' + fallbacks[i], function(img) {
            fallbackImgs[i] = img;
          })
        })(i);
      }
    } else {
      resolve();
    }
  });
}

function createPuzzle(options) {
  return new Promise(function(resolve, reject) {
    var config = {
      fov: 30,
      offsets: [
        new THREE.Vector3(-0.3, 0.3, 0),
        new THREE.Vector3(-10, 0.3, 0),
        new THREE.Vector3(-21, 1, 0)
      ],
      scale: 0.05,
      inertialK: 0.05,
      sphereRMul: 0.01,
      thickness: 7.5,
      deviceOrientationSensitivity: 9
    };

    var now = {
      frame: 0,
      animFrame: 0,
      x: 0.5,
      y: 0.5,
      k: 1,
      transitionActive: false,
      currentType: 0,
      targetType: 0,
      transitionK: 0,
      dR: 0,
      revealed: false,
    };

    var rafInstance;
    var renderer;
    var materials = [];
    var initialPoints = [];
    var meshes = [];
    var geos = [];
    var initials = [];
    var sphere;
    var cameraOrtho;
    var composeScene;
    var camera;
    var scene;
    var controls;
    var cube;
    var iTextures;

    var puzzle = {
      generateInitialPoints: function(startType, states) {
        var lerp = function(a, b, k) {
          return a * (1 - k) + b * k;
        }
        initialPoints[0] = [];
        for (var m = 0; m < 2; m++) {
          initialPoints[0][m] = [];
          var num = 100;
          for (var i = 0; i < num; i++) {
            var part = i / (num - 1);
            var a = - part * Math.PI * 4.5;
            var r = 0 + 60 * (1 - m);
            var p = {
              x: Math.cos(a) * r,
              y: -50 + part * 100,
              z: Math.sin(a) * r
            };
            p.y *= (1 - m * 0.83);
            initialPoints[0][m].push(p);
          }
        }
        for (var t = 1; t < config.introStates; t++) {
          initialPoints[t] = [];
          for (var m = 0; m < 2; m++) {
            initialPoints[t][m] = [];
            var num = 100;
            var ps0 = initialPoints[0][m];
            var ps1 = curves[startType][m];
            for (var i = 0; i < num; i++) {
              var p0 = ps0[i];
              var p1 = ps1[i];
              var p = {
                x: lerp(p0.x, p1.x, t / config.introStates),
                y: lerp(p0.y, p1.y, t / config.introStates),
                z: lerp(p0.z, p1.z, t / config.introStates),
              };
              initialPoints[t][m].push(p);
            }
          }
        }
      },
      generateInitials: function() {
        for (var k = 0; k < initialPoints.length; k++) {
          initials[k] = [];
          for (var m = 0; m < 2; m++) {
            var points = initialPoints[k][m];
            initials[k][m] = puzzle.createGeometry(points);
            initials[k][m].dataTextures = puzzle.createDataTex(initials[k][m]);
          }
        }
      },
      init: function(options) {
        Object.assign(config, options);
        puzzle.canvas = options.canvas;
        config.detalization = options.detalization > 1 ? 1 : (options.detalization || 1);
        config.transChangeInt = options.autoTransitionInterval ? options.autoTransitionInterval * 60 : null;
        config.introIntermediates = options.introIntermediates || 0;
        config.introStates = config.introIntermediates + 2;
        config.intelSafariFading = !!options.intelSafariFading;

        now.isFall = !webgl || options.forceFallbackOnMobile && isTouchDevice || options.forceFallback;

        if (now.isFall) {
          now.fallback = true;
          puzzle.canvas.style.backgroundImage = 'url(' + assetsPath + '/fallback/' + options.fallback + ')';
          puzzle.canvas.style.backgroundPosition = 'center';
          puzzle.canvas.style.backgroundSize = 'cover';
          return;
        }

        if (config.introRequired) {
          puzzle.generateInitialPoints(options.startType, 4);
          puzzle.generateInitials();
        }

        for (var matName in materialsShared) {
          if (materialsShared.hasOwnProperty(matName)) {
            materials[matName] = materialsShared[matName].clone();
          }
        }

        now.viewVTarget = now.viewV = options.viewV;
        now.viewHTarget = now.viewH = options.viewH;
        now.viewRTarget = now.viewR = options.viewR;    

        cameraOrtho = new THREE.OrthographicCamera(0, 1, 1, 0, -10, 10);
        cameraOrtho.position.z = 2;
        camera = new THREE.PerspectiveCamera(config.fov, config.canvas.width / config.canvas.height, 0.01, 100);
        camera.position.set(0, 0, 5);
        camera.prevPosition = new THREE.Vector3();
        camera.lookAt(new THREE.Vector3(0.5, 0.5, 0.5));
        composeScene = new THREE.Scene();
        scene = new THREE.Scene();
        window.scene2 = new THREE.Scene();

        // cube = cubeShared.clone();

        renderer = new THREE.WebGLRenderer({
          alpha: true,
          canvas: config.canvas,
          // autoClear: now.renderVersion !== 3,
          antialias: true,
          premultipliedAlpha: false,
          preserveDrawingBuffer: !!config.preserveDrawingBuffer
        });
        renderer.getContext().enable(renderer.getContext().DITHER);
        renderer.setPixelRatio(window.devicePixelRatio ? window.devicePixelRatio : 1);

        for (var m = 0; m < 2; m++) {
          geos[m] = [];
          for (var i = 0; i < 3; i++) {
            geos[m][i] = puzzle.createGeometry(curves[i][m]);
            geos[m][i].dataTextures = puzzle.createDataTex(geos[m][i]);
          }
        }

        if (config.transitionsRequired) {
          iTextures = [];
          for (var m = 0; m < 2; m++) {
            iTextures[m] = [];
            for (var i = 0; i < 3; i++) {
              for (var j = 0; j < 3; j++) {
                if (i === j) continue;
                (function(i, j, m) {
                  var done = false;
                  for (var t = 0; t < iTextures[m].length; t++) {
                    if ((iTextures[m][t].one === i || iTextures[m][t].two === i) && (iTextures[m][t].one === j || iTextures[m][t].two === j)) {
                      done = true;
                      break;
                    }
                  }
                  if (!done) {
                    var geo = puzzle.createGeometry(curves[i][m], curves[j][m]);
                    var data = puzzle.createDataTex(geo);
                    iTextures[m].push({
                      one: i,
                      two: j,
                      data: data
                    });
                  }
                })(i, j, m);
              }
            }
          }
        }

        for (var m = 0; m < 2; m++) {
          meshes[m] = new THREE.Mesh(geos[m][config.startType], materials['pipe'].clone());
          meshes[m].scale.set(config.scale, config.scale, config.scale);
          scene.add(meshes[m]);
        }

        var sphGeometry = new THREE.SphereGeometry( config.thickness * 0.06, 16, 16);
        sphere = new THREE.Mesh( sphGeometry, materials['pipeNoTex'] );
        sphere.scale.set(0.0001, 0.0001, 0.0001);
        scene.add(sphere);

        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.target.set(0, 0, 0);
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;

        puzzle.animate();
        puzzle.resize(null, null, true);

        now.currentType = config.startType;
        now.targetType = config.startType;

        puzzle.debug.mixWhite = config.mixWhite || 0;

        if (isTouchDevice && window.DeviceOrientationEvent && config.deviceOrientationSensitivity) {
          window.addEventListener('deviceorientation', function(e) {
            var tiltLR = e.gamma * config.deviceOrientationSensitivity;
            var tiltFB = e.beta * config.deviceOrientationSensitivity;
            puzzle.setPointer(- tiltLR / 30 + 0.5, - (tiltFB - 25) / 30 + 0.5);
          }, false);
        }

        if (isTouchDevice) {
          // config.pointerImpact *= 1;
          config.inertialK = 1;
        }

        resolve(puzzle);
      },
      debug: {
        controls: false,
        cubeMul: 3,
        fresnelBias: 1,
        fresnelScale: 1,
        fresnelPower: 1,
        lightX: 1,
        lightY: 1,
        lightZ: 1,
        lightFactor: 0.001,
        addition: 0.001,
        cubeAddition: -1,
        mixWhite: 0.8,
        contrast: 4,
        psychoFreq: 0.003,
        psychoPower: 0,
        morphType: 0.001,
      },
      createGeometry: function(points, points2) {
        var total = points.length;
        var num = Math.round(total);
        var newPoints = [];
        if (num < 2) num = 2;
        if (points2) {
          for (var i = 0; i < num; i++) {
            newPoints[i] = {
              x: (points[i].x + points2[i].x) / 2 + 2,
              y: (points[i].y + points2[i].y) / 2,
              z: (points[i].z + points2[i].z) / 2
            };
          }
        } else {
          for (var i = 0; i < num; i++) {
            newPoints[i] = points[i];
          }
        }
        var spline = new THREE.SplineCurve3(newPoints);
        if (isTouchDevice && config.detalization > 0.7) {
          config.detalization = 0.7;
        }
        var segNum = Math.floor(config.detalization * 410);
        var secNum = Math.floor(config.detalization * 12);
        var curveGeometry = new THREE.TubeGeometry(
          spline,
          segNum,
          config.thickness,
          secNum,
          points.closed
        );
        var numpoints = curveGeometry.numpoints;
        var numpointsTotal = curveGeometry.faces.length * 3;
        var vIndicesV = new Float32Array( numpointsTotal );
        for (var i = 0; i < curveGeometry.faces.length; i++) {
          var indV = i * 3;
          vIndicesV[indV] = indV;
          vIndicesV[indV + 1] = indV + 1;
          vIndicesV[indV + 2] = indV + 2;
        }
        curveGeometry = new THREE.BufferGeometry().fromGeometry( curveGeometry );
        curveGeometry.numpoints = numpoints;
        curveGeometry.numpointsTotal = numpointsTotal;
        curveGeometry.addAttribute( 'vertIndexV', new THREE.BufferAttribute( vIndicesV, 1 ) );
        var sp = newPoints[0];
        var ep = newPoints[newPoints.length - 1];
        curveGeometry.startPoint = new THREE.Vector3(sp.x, sp.y, sp.z);
        curveGeometry.endPoint = new THREE.Vector3(ep.x, ep.y, ep.z);
        curveGeometry.startPoint.multiplyScalar(config.scale);
        curveGeometry.endPoint.multiplyScalar(config.scale);
        return curveGeometry;
      },
      createDataTex: function(geo) {
        var nearestPow2 = function( size ){
          return Math.pow( 2, Math.ceil( Math.log( size ) / Math.log( 2 ) ) ); 
        }
        var positions = geo.attributes.position;
        var normals = geo.attributes.normal;
        var side = nearestPow2(Math.pow(positions.count, 0.5));
        config.side = side;
        var posDataArray = new Float32Array( side * side * 3 ); 
        var normalArray = new Float32Array( side * side * 3 ); 
        for (var y = 0; y < side; y++) {
          for (var x = 0; x < side; x++) {
            var index = y * side * 3 + x * 3;
            if (y * side + x >= positions.count) break;
            posDataArray[index] = positions.array[index]; 
            posDataArray[index + 1] = positions.array[index + 1]; 
            posDataArray[index + 2] = positions.array[index + 2]; 
            normalArray[index] = normals.array[index]; 
            normalArray[index + 1] = normals.array[index + 1]; 
            normalArray[index + 2] = normals.array[index + 2]; 
          }
        }
        var dataTextures = {};
        dataTextures.dataTex = new THREE.DataTexture(
          posDataArray,
          side,
          side,
          THREE.RGBFormat,
          THREE.FloatType,
          null,
          THREE.ClampToEdgeWrapping,
          THREE.ClampToEdgeWrapping,
          THREE.NearestFilter,
          THREE.NearestFilter
        );
        dataTextures.dataTex.needsUpdate = true;
        dataTextures.normalTex = new THREE.DataTexture(
          normalArray,
          side,
          side,
          THREE.RGBFormat,
          THREE.FloatType,
          null,
          THREE.ClampToEdgeWrapping,
          THREE.ClampToEdgeWrapping,
          THREE.NearestFilter,
          THREE.NearestFilter
        );
        dataTextures.normalTex.needsUpdate = true;
        return dataTextures;
      },
      fetchIData: function(m, i, j) {
        for (var t = 0; t < iTextures[m].length; t++) {
          if ((iTextures[m][t].one === i || iTextures[m][t].two === i) && (iTextures[m][t].one === j || iTextures[m][t].two === j)) {
            return iTextures[m][t].data;
          }
        }
      },
      setTextures: function(n, k, i) {
        var tk = k > 1 ? 1 : k;
        var u = meshes[n].material.uniforms;
        var data0;
        var data1;
        if (typeof i === 'number') {
          var step = 1 / config.introStates;
          for (var q = 0; q < config.introStates; q++) {
            if (tk <= step * (q + 1)) {
              data0 = initials[q][i].dataTextures;
              if (initials[q + 1]) {
                data1 = initials[q + 1][i].dataTextures;
              } else {
                data1 = geos[n][now.targetType].dataTextures;
              }
              tk = (tk - q / config.introStates) * config.introStates;
              break;
            }
          }
        } else {
          if (now.currentType === now.targetType) {
            data0 = geos[n][now.currentType].dataTextures;
            data1 = geos[n][now.targetType].dataTextures;
          } else {
            if (tk < 0.5) {
              data0 = geos[n][now.currentType].dataTextures;
              data1 = puzzle.fetchIData(n, now.currentType, now.targetType);
              tk *= 2;
            } else {
              data0 = puzzle.fetchIData(n, now.currentType, now.targetType);
              data1 = geos[n][now.targetType].dataTextures;
              tk = (tk - 0.5) * 2;
            }
          }
        }
        u['morph'].value = tk;

        u['uPosTex1'].value = data0.dataTex;
        u['uPosTex2'].value = data1.dataTex;

        u['uNormTex1'].value = data0.normalTex;
        u['uNormTex2'].value = data1.normalTex;
      },
      setState: function(type) {
        now.animation = null;
        now.transitionActive = false;
        now.transitionK = 0;
        now.currentType = typeof type === 'number' && type || config.startType;
        now.targetType = now.currentType;
        now.animFrame = 0;
        // now.viewVTarget = now.viewV = config.viewV;
        // now.viewHTarget = now.viewH = config.viewH;
        // now.viewRTarget = now.viewR = config.viewR; 
        meshes[0].rotation.set(0,0,0);
        meshes[1].rotation.set(0,0,0);
      },
      render: function() {
        for (var n = 0; n < 2; n++) {
          var u = meshes[n].material.uniforms;
          u['dataSide'].value = config.side;
          u['envMap'].value = cubeShared;
          u['cubeMul'].value = puzzle.debug.cubeMul;
          u['cubeAddition'].value = puzzle.debug.cubeAddition;
          u['addition'].value = puzzle.debug.addition;
          u['mixWhite'].value = puzzle.debug.mixWhite;
          u['contrast'].value = puzzle.debug.contrast;
          u['uNumpoints'].value = meshes[n].geometry.numpoints;
          u['opacity'].value = 1;
          if (!now.animation) {
            puzzle.setTextures(n, now.transitionK);
          }
        }

        var su = sphere.material.uniforms;
        var spos = spherePositions[now.currentType].clone().lerp(spherePositions[now.targetType], now.transitionK);
        sphere.position.copy(spos);
        sphere.scale.set(1, 1, 1);
        su['envMap'].value = cubeShared;
        su['cubeMul'].value = puzzle.debug.cubeMul;
        su['cubeAddition'].value = puzzle.debug.cubeAddition;
        su['addition'].value = puzzle.debug.addition;
        su['mixWhite'].value = puzzle.debug.mixWhite;
        su['contrast'].value = puzzle.debug.contrast;
        su['opacity'].value = 1;

        if (now.transitionActive) {
          var nowT = new Date().getTime();
          var t = nowT - now.startT;
          now.transitionK = easings['easeInOutCubic'](null, t, 0, 1, now.finishT - now.startT);
          if (t > now.finishT - now.startT) {
            now.transitionActive = false;
            now.transitionK = 0;
            now.currentType = now.targetType;
            if (typeof config.onTransitionFinish === 'function') {
              config.onTransitionFinish(puzzle);
            }
          }
        }

        if (now.animation) {
          now.currentType = 0;
          now.targetType = config.startType;
          if (now.k > 1) {
            now.k = 1;
            now.animation = false;
            now.currentType = now.targetType;
            puzzle.revealed = true;
            if (typeof config.onIntroFinish === 'function') {
              config.onIntroFinish(puzzle);
            }
          }
          var k = easings['easeInOutQuad'](null, now.k, 0, 1, 1);
          var faded = false;
          var opacity = 1;
          if (now.fadeK < 1) {
            now.fadeK += 0.02;
          } else {
            now.fadek = 1;
            faded = true;
          }
          if (config.startType === 0) {
            var xrot = 3;
            var yrot = 0.15;
            var zrot = 0.15;
            var sphY = 1.5;
          }
          if (config.startType === 1) {
            var xrot = 2;
            var yrot = 0.1;
            var zrot = 0.15;
            var sphY = -4;
          }
          if (config.startType === 2) {
            var xrot = 3;
            var yrot = -0.1;
            var zrot = -0.;
            var sphY = -3;
          }
          if (isIntelSafari && config.intelSafariFading) {
            var white = config.mixWhite + (1 - config.mixWhite) * (1 - k);
            for (var n = 0; n < 2; n++) {
              var u = meshes[n].material.uniforms;
              var data0 = geos[n][config.startType].dataTextures;
              var data1 = geos[n][config.startType].dataTextures;
              u['uPosTex1'].value = data0.dataTex;
              u['uPosTex2'].value = data1.dataTex;
              u['uNormTex1'].value = data0.normalTex;
              u['uNormTex2'].value = data1.normalTex;
              u['mixWhite'].value = white;
            }
            su['mixWhite'].value = white;
            now.viewR = config.viewR;
            var spos = spherePositions[config.startType].clone();
            sphere.position.copy(spos);
          } else {
            opacity = easings['easeOutQuad'](null, now.fadeK, 0, 1, 1);
            for (var n = 0; n < 2; n++) {
              var u = meshes[n].material.uniforms;
              var tk = k < 0.5 ? k * 2 : (k - 0.5) * 2;
              u['morph'].value = tk;
              u['opacity'].value = opacity;
              u['mixWhite'].value = k * config.mixWhite;
              puzzle.setTextures(n, k, n);
              if (config.startType > 0 || n === 0) {
                meshes[n].rotation.y = (-1 + k) * Math.PI * xrot;
                meshes[n].rotation.x = (-1 + k) * Math.PI * yrot;
                meshes[n].rotation.z = (-1 + k) * Math.PI * zrot;
              }
            }
            su['mixWhite'].value = k * config.mixWhite;
            su['opacity'].value = opacity;
            var sscale = k + 0.00001;
            sphere.scale.set(sscale, sscale, sscale);
            var a = Math.PI / 2 * (1 - k);
            var r = spherePositions[config.startType].x;
            sphere.position.set(Math.cos(a) * r, spherePositions[config.startType].y + (1 - k) * sphY, Math.sin(a) * r);
            now.viewR = config.viewR + (1 - k) * 12 + now.dR;
          }
          if (faded) {
            now.k += now.k < 0.2 ? 0.006 : 0.008;
          } else {
            now.k += 0.003;
          }
        }
        var cc = new THREE.Color(0xffffff);
        renderer.setClearColor( cc, 1);
        renderer.render(scene, camera);

        now.frame++;
      },
      reveal: function() {
        if (!config.introRequired) return;
        now.animation = true;
        now.fadeK = 0;
        now.k = 0;
        now.active = true;
        now.viewR = config.viewR + 10 + now.dR;
      },
      transition: function(type) {
        if (now.transitionActive) return;
        if (!config.transitionsRequired) return;
        now.transitionActive = true;
        now.startT = new Date().getTime();
        now.finishT = now.startT + 1000;
        if (typeof type === 'number') {
          now.targetType = type;
        } else {
          do {
            now.targetType = Math.floor(Math.random() * 3);
          } while (now.targetType === now.currentType);
        }
      },
      animate: function() {
        if (now.fallback) return;
        rafInstance = window.requestAnimationFrame(puzzle.animate);
        var t = new Date().getTime();
        if (isTouchDevice) {
          if (now.needsResize && t > now.resizeTime) {
            puzzle.resizeLocal();
            now.needsResize = false;
          }
        }
        if (puzzle.debug.controls) {
          controls.enabled = true;
          controls.update();
        }
        if (!puzzle.debug.controls) {
          controls.enabled = false;
        }
        
        if (!now.active && !now.needsUpdate) {
          return;
        }
        var camShift = camera.prevPosition.distanceTo(camera.position);
        if (!controls.enabled) {
          puzzle.updateCamera();
        }
        if (config.transitionsRequired && config.transChangeInt && (now.animFrame % config.transChangeInt === 0)) {
          puzzle.transition();
        }
        if (camShift < 0.001 && !now.animation && !now.transitionActive) {
          return;
        }
        now.animFrame ++;
        puzzle.render();
        now.needsUpdate = false;
      },
      updateCamera: function() {
        camera.prevPosition = camera.position.clone();
        var dH = (now.x) * 0.2 * (isTouchDevice ? 1 : camera.aspect) || 0;
        var dV = (now.y) * 0.2 || 0;
        dH *= config.pointerImpact;
        dV *= config.pointerImpact;
        now.viewH += (now.viewHTarget - dH - now.viewH) * config.inertialK;
        now.viewV += (now.viewVTarget - dV - now.viewV) * config.inertialK;
        now.viewR += (now.viewRTarget + now.dR - now.viewR) * config.inertialK;
        var vz = now.viewR * Math.sin(now.viewV) * Math.cos(now.viewH);
        var vx = now.viewR * Math.sin(now.viewV) * Math.sin(now.viewH);
        var vy = now.viewR * Math.cos(now.viewV);
        camera.position.set(vx, vy, vz);
        camera.lookAt(new THREE.Vector3(0, 0, 0));
      },
      play: function() {
        now.active = true;
      },
      pause: function() {
        now.active = false;
        if (now.transitionActive) {
          puzzle.setState(now.currentType);
          puzzle.render();
          now.animFrame ++;
        }
       },
      setPointer: function(x, y) {
        if (now.fallback) return;
        // if (x > 0 && y > 0 && x < 1 && y < 1) {
          now.x = x - 0.5;
          now.y = y - 0.5;
          var v = new THREE.Vector3(now.x, now.y, 0);
          var rotation = new THREE.Matrix4().makeRotationZ(config.pointerRotation);
          v.applyMatrix4(rotation);
          now.x = v.x;
          now.y = v.y;
        // }
      },
      resizeLocal: function() {
        camera.aspect = now.w / now.h;
        camera.updateProjectionMatrix();
        var pixelRatio = renderer.getPixelRatio();
        var sizeMultiplier = pixelRatio > 1 ? (config.detalization < 0.9 ? 1.4 : 1) : 1.5;
        renderer.setSize(now.w * sizeMultiplier, now.h * sizeMultiplier, false);
        renderer.domElement.style.width = now.w + 'px';
        renderer.domElement.style.height = now.h + 'px';
        now.x = 0.5;
        now.y = 0.5;
        var aspect = now.w / now.h;
        if (aspect < 1) {
          now.dR = 1 - aspect;
          now.dR *= 40;
        } else {
          now.dR = 0;
        }
      },
      resize: function(w, h, force) {
        if (now.fallback) return;
        now.w = w || parseInt(config.canvas.clientWidth, 10);
        now.h = h || parseInt(config.canvas.clientHeight, 10);
        if (isTouchDevice && force !== true) {
          var t = new Date().getTime();
          now.resizeTime = t + 100;
          now.needsResize = true;
        } else {
          puzzle.resizeLocal();
        }
      }
    };

    puzzle.now = now;
    puzzle.init(options);
  });
}

function loadJSON(file, callback) {   
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', file, true); 
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
          callback(xobj.responseText);
        }
    };
    xobj.send(null);  
 }

module.exports = {
  load: load,
  create: createPuzzle,
  fallbackFiles: fallbackFiles
}