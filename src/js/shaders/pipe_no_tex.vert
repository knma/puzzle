precision highp float;

// varying float vReflectionFactor;
varying vec3 vI;
varying vec3 vWorldNormal;

// varying vec3 vNormal;
varying float vOpacity;

void main() {
  vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
  vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
  vWorldNormal = normalize( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );
  vI = worldPosition.xyz - cameraPosition;
  // vReflectionFactor = fresnelBias + fresnelScale * pow( 1.0 + dot( normalize( vI ), vWorldNormal ), fresnelPower );

  gl_Position = projectionMatrix * mvPosition;

  vOpacity = 1.;
}