precision highp float;

attribute float vertIndexV;

uniform float dataSide;
uniform float uNumpoints;
uniform float morph;
uniform sampler2D uPosTex1;
uniform sampler2D uPosTex2;
uniform sampler2D uNormTex1;
uniform sampler2D uNormTex2;

varying vec3 vI;
varying vec3 vWorldNormal;

varying float vOpacity;

void main() {

  // float mval = cos(morph * 3.1415 * 2.) * 0.5 + 0.5;
  // mval = 1. - mval;
  // float _k = vertIndexV / uNumpointsTotal;

  vec2 uv;
  uv.y = vertIndexV / dataSide / dataSide;
  uv.x = mod(vertIndexV, dataSide) / dataSide;
  
  vec3 pos1 = texture2D(uPosTex1, uv).rgb;
  vec3 pos2 = texture2D(uPosTex2, uv).rgb;

  vec3 norm1 = texture2D(uNormTex1, uv).rgb;
  vec3 norm2 = texture2D(uNormTex2, uv).rgb;

  vec3 pos = mix(pos1, pos2, morph);
  vec3 vNormal = mix(norm1, norm2, morph);

  vec4 mvPosition = modelViewMatrix * vec4( pos, 1.0 );
  vec4 worldPosition = modelMatrix * vec4( pos, 1.0 );
  vWorldNormal = normalize( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * vNormal );
  vI = worldPosition.xyz - cameraPosition;

  gl_Position = projectionMatrix * mvPosition;

  vOpacity = 1.;
}