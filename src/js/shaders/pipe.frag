precision highp float;

uniform samplerCube envMap;

uniform float cubeMul;
uniform float cubeAddition;
uniform float addition;
uniform float mixWhite;
uniform float contrast;
uniform float opacity;

varying vec3 vI;
varying vec3 vWorldNormal;

varying float vOpacity;

vec3 _contrast(vec3 value, float _contrast)
{
    return (value - 0.5) * _contrast + 0.5;
}

void main() {

  vec3 reflection = reflect( vI, vWorldNormal );
  vec4 envColor = textureCube( envMap, vec3( -reflection.x, reflection.yz ) ) * cubeMul;
  envColor += cubeAddition;

  // vec3 col = vec3(1.);
  gl_FragColor.rgb = envColor.xyz;

  gl_FragColor += addition;

  gl_FragColor.rgb = _contrast(gl_FragColor.rgb, contrast);
  
  gl_FragColor.rgb = mix(clamp(gl_FragColor.rgb, 0., 1.), vec3(1.), mixWhite);

  gl_FragColor.a = vOpacity * opacity;

}